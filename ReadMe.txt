Simon Horridge - Exercise for HSBC
In the event of any query, please call me on 07879 4971925 or 07922 628938

When web app starts, go to localhost:(port)/index.html

Data is stored in local Data source. In a real world situation, the datastore would be a proper DB.

There is a reset test button, which will delete all data in the local data store.

For a real application, more time would be spend on User Experience - eg spinners displayed while data is loading,
Upload file to be displayed in modal/ lightbox. 
Validation would be done on files uploaded. 


Client-side code is using angularJS - all functionality is in app.js. 
In a real world situation the code would be split in to modules, and bundled/ minified at compilation/ release.


Server side code design desisions - 

In order that code is maintainable, classes are small and have a single responsibility.

To reduce coupling, dependency injection should be used.

In a real world situation, a mock would be created to test data inserts.
Similarly, a test would be written to test uploading and parsing the csv file, alos negative tests would be created - ie test that uploading malformed csv throws correct exception.

For reasons of expediency, there is no validation of csv files, uploaded files are assumed to be correct.