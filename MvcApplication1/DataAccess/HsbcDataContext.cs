using System.Data.Entity;
using Hsbc.DataAccess.Entities;

// TODO : use this to store data.
namespace Hsbc.DataAccess
{
    public class HsbcDataContext : DbContext
    {
        // Your context has been configured to use a 'HsbcDataModel' connection string from your application's 
        // configuration file (App.config or Web.config). By default, this connection string targets the 
        // 'hsbc.HsbcDataModel' database on your LocalDb instance. 
        // 
        // If you wish to target a different database and/or database provider, modify the 'HsbcDataModel' 
        // connection string in the application configuration file.
        public HsbcDataContext()
            : base("name=HsbcDataModel")
        {
        }

        // Add a DbSet for each entity type that you want to include in your model. For more information 
        // on configuring and using a Code First model, see http://go.microsoft.com/fwlink/?LinkId=390109.

        public virtual DbSet<StockValue> StockValues { get; set; }
    }
}