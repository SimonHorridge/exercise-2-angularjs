using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Hsbc.DataAccess.Entities
{
    public class StockValue
    {
        [Key]
        [Column(Order = 1)] 
        public string IndexName { get; set; }
        
        [Key]
        [Column(Order = 2)] 
        public DateTime Date { get; set; }

        [Key]
        [Column(Order = 3)]
        public string StockId { get; set; }

        public decimal Price { get; set; }

        public string Name { get; set; }

        public int NumberOfShares { get; set; }

        public decimal TotalValue { get; set; }    
    }
}