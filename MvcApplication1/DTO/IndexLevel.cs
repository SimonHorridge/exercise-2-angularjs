﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Hsbc.DTO
{
    public class IndexLevel
    {
        public string IndexName { get; set; }
        public DateTime Date { get; set; }
        public decimal Value { get; set; }
        public decimal NormalisedValue { get; set; }
    }
}