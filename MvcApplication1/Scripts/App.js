﻿(function () {
    "use strict";

    var app = angular.module('app', []);

    // todo : use datepicker for dates

    var unwatchDateFrom;
    var unwatchDateTo;
    var dataTable;
    app.controller('indexController', function ($scope, $http) {
        $scope.uploadFile = uploadFile;
        $scope.loadData = loadData;
        $scope.resetExercise = resetExercise;
        $scope.message = "Loading, please wait";

        $scope.dataUploaded = false;

        loadPageData();

        // todo - dateFrom and dateTo are both updated by loadPageData -  causing data to be loaded twice.

        function loadPageData() {
            $http.get("/api/Index/GetPageData")
                .success(function (data) {
                    $scope.message = "Please select a csv file to upload, then click Upload File"
                    if (unwatchDateFrom && typeof unwatchDateFrom === "function") {
                        unwatchDateFrom();
                    }
                    if (unwatchDateTo && typeof unwatchDateTo === "function") {
                        unwatchDateTo();
                    }

                    $scope.dataExists = data.dataExists;
                    $scope.minDate = new Date(data.minDate);
                    $scope.dateFrom = new Date( data.minDate);
                    $scope.maxDate = new Date(data.maxDate);
                    $scope.dateTo = new Date(data.maxDate);

                    // todo : validate dates  - well formed, within range, dateFrom before dateTo             
                    // todo - replace with datepicker directive
                    $("#dateFrom").datepicker("setDate", $scope.dateFrom).datepicker("option", "maxDate", $scope.maxDate).datepicker("option", "minDate", $scope.minDate);
                    $("#dateTo").datepicker("setDate", $scope.dateTo).datepicker("option", "maxDate", $scope.maxDate).datepicker("option", "minDate", $scope.minDate);

                    unwatchDateFrom =  $scope.$watch("dateFrom", function (newVal, oldVal) {
                        if (newVal && newVal != oldVal) {
                            loadData();
                        }
                    });
                    unwatchDateTo = $scope.$watch("dateTo", function (newVal, oldVal) {
                        if (newVal && newVal != oldVal) {
                            loadData();
                        }
                    });
                    loadData();
                });
        }

        // todo: this should be in service and injected
        function uploadFile() {
            var fileElement = $("#file");
            if (fileElement.length && fileElement[0].files && fileElement[0].files.length) {
                var fd = new FormData();
                fd.append("uploadedFile", fileElement[0].files[0]);
                var xhr = new XMLHttpRequest();
                xhr.addEventListener("load", uploadComplete, false);
                xhr.addEventListener("error", uploadFailed, false);
                xhr.open("POST", "/api/Index/Post");
                xhr.send(fd);
            } else {
                alert("Please select a file");
            }
        }

        function uploadComplete(data) {
            $scope.dataUploaded = true;
            loadPageData();
        }
        function loadData() {
            $.get("/api/Index/GetStockValues?dateFrom=" + $scope.dateFrom + "&dateTo=" + $scope.dateTo, displayData);
            $.get("/api/Index/GetTopWeightedStocks?date=" + $scope.dateTo, displayTopPerformingStocks);
            $.get("/api/Index/GetIndexLevels?dateFrom=" + $scope.dateFrom + "&dateTo=" + $scope.dateTo, displayIndexLevels);            
        }
        function displayData(data) {
            if (data && data.length) {
                dataTable = $('#dataTable').dataTable({
                    "data": data.map(function (x) {
                        return [x.IndexName, x.Date, x.StockId, x.Price, x.Name, x.NumberOfShares, x.TotalValue];
                    }),
                    destroy: true
                });
            }
        }

        function displayTopPerformingStocks(data) {
            $('#topWeightedStocks').highcharts({
                chart: {
                    plotBackgroundColor: null,
                    plotBorderWidth: null,
                    plotShadow: false
                },
                title: {
                    text: 'Top 5 Weighted stocks on ' + $scope.dateTo
                },
                tooltip: {
                    pointFormat: '{series.name}: <b>{point.y}</b>'
                },
                plotOptions: {
                    pie: {
                        allowPointSelect: true,
                        cursor: 'pointer',
                        dataLabels: {
                            enabled: true,
                            format: '<b>{point.name}</b>: {point.y}',
                            style: {
                                color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black'
                            }
                        }
                    }
                },
                series: [{
                    type: 'pie',
                    name: 'Stock Total Value',
                    data: createPieChart(data) 
                }]
            });
        }

        function createPieChart(data) {
            return data.map(function (x) {
                return [x.Name, x.TotalValue];
            });
        }

        function resetExercise() {
            $.get("/api/Index/ResetExercise", function () {
                location.reload();
            });
        }

        function displayIndexLevels(data) {
            //TODO : work put how to format date for y axis correctly.            
            var chartdata = {
                chart: {
                    type: 'spline'
                },
                title: {
                    text: 'Index Values'
                },
                xAxis: {
                    type: 'datetime',

                    title: {
                        text: 'Date'
                    }
                },
                yAxis: {
                    title: {
                        text: 'Index Value'
                    },
                    min: 0
                },
                tooltip: {
                    headerFormat: '<b>{series.name}</b><br>',
                    pointFormat: '{point.x:%e. %b}: {point.y:.2f}'
                },

                plotOptions: {
                    spline: {
                        marker: {
                            enabled: true
                        }
                    }
                },

                series: createSeries(data)
            }

            $('#indexChart').highcharts(chartdata);
        };

        function createSeries(data) {
            var rtn = data.map(function (x) {
                return {
                    name: x.indexName, data:
                        x.values.map(function (val) {
                            return [Date.parse(val.Date), val.NormalisedValue];
                        })
                };
            });
            return rtn;
        }

        function uploadFailed(data) { alert("upload Failed"); }
    });
}())