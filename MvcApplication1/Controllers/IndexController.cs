﻿using Hsbc.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Security.Policy;
using System.Web;
using System.Web.Http;
using Hsbc.DataAccess;
using Hsbc.DataAccess.Entities;
using Hsbc.DTO;

namespace Hsbc.Controllers
{
    public class IndexController : ApiController
    {
        private readonly HsbcDataContext _context = new HsbcDataContext();
        private CapitalizationWeightedIndexService _capitalizationWeightedIndexService;
        public IndexController()
        {
            _capitalizationWeightedIndexService = new CapitalizationWeightedIndexService();
        }


        [HttpGet]
        public void ResetExercise()
        {
            foreach (var item in _context.StockValues.ToList())
            {
                _context.StockValues.Remove(item);
            }
            _context.SaveChanges();
        }


        [HttpGet]
        public object GetPageData()
        {
            var dataExists = _context.StockValues.Any();
            var minDate = dataExists ? (DateTime?)_context.StockValues.Select(x => x.Date).Min().Date : null;
            var maxDate = dataExists ? (DateTime?)_context.StockValues.Select(x => x.Date).Max().Date : null;

            return new
            {
                dataExists,
                maxDate,
                minDate
            };
        }

        [HttpGet]
        public List<StockValue> GetStockValues(DateTime? dateFrom = null, DateTime? dateTo = null)
        {
            var query = _context.StockValues.AsQueryable();
            if (dateFrom != null)
            {
                query = query.Where(x => x.Date >= dateFrom.Value);
            }
            if (dateTo != null)
            {
                query = query.Where(x => x.Date <= dateTo.Value);
            }

            var rtn = query.ToList();
            return rtn;
        }


        [HttpGet]
        public List<StockValue> GetTopWeightedStocks(DateTime? date = null, int take = 5)
        {
            if (!_context.StockValues.Any())
            {
                return new List<StockValue>();
            }

            var dateToUse = date ?? _context.StockValues.Select(x => x.Date).Max().Date;

            var rtn = _context.StockValues
                .Where(x => x.Date == dateToUse)
                .OrderByDescending(x => x.TotalValue)
                .Take(5)
                .ToList();

            return rtn;
        }

        [HttpGet]
        public List<object> GetIndexLevels(DateTime? dateFrom = null, DateTime? dateTo = null)
        {
            var query = _context.StockValues.AsQueryable();
            if (dateFrom != null)
            {
                query = query.Where(x => x.Date >= dateFrom.Value);
            }
            if (dateTo != null)
            {
                query = query.Where(x => x.Date <= dateTo.Value);
            }

            // TODO : precalculate values. 
            var interim = query.GroupBy(x => new { x.IndexName, x.Date }, (group, val) => new IndexLevel { Date = group.Date, IndexName = group.IndexName, Value = val.Sum(x => x.TotalValue) }).ToList();

            if (interim.Any())
            {
                var value0 = interim[0].Value;
                interim.ForEach(x => x.NormalisedValue = 100 * x.Value / value0);
            }

            var rtn = interim.GroupBy(x => x.IndexName, (group, val) => new { indexName = @group, values = val }).ToList<object>();
            return rtn;
        }

        [HttpPost]
        public HttpResponseMessage Post()
        {
            //TODO - store file in more sensible location
            //TODO - ensure data does not already exist/ deal with dupes
            //TODO - validate file, Handle large files/ other file types/ zipped files
            //TODO - refactor - put code to save file in separate class (Single responsibility principle)
            //TODO - create unit tests
            //TODO - deal with pk violation or other exceptions - currently returns 200 OK

            HttpResponseMessage result = null;
            var httpRequest = HttpContext.Current.Request;
            if (httpRequest.Files.Count > 0)
            {
                foreach (string file in httpRequest.Files)
                {
                    var postedFile = httpRequest.Files[file];
                    var filePath = HttpContext.Current.Server.MapPath("~/" + postedFile.FileName);
                    postedFile.SaveAs(filePath);
                    var fileSourceType = CapitalizationWeightedIndexService.FileSourceType.Csv; //ToDo : identify other filetypes
                    _capitalizationWeightedIndexService.ProcessFile(filePath, x => _context.StockValues.Add(x), fileSourceType);
                    _context.SaveChanges();
                }
                result = Request.CreateResponse(HttpStatusCode.Created);
            }
            else
            {
                result = Request.CreateResponse(HttpStatusCode.BadRequest);
            }

            return result;
        }
    }
}
