﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using Hsbc.DataAccess;
using Hsbc.DataAccess.Entities;

namespace Hsbc.Domain
{
    // class is public so can be tested. In real world would be private. Tests would be configured to handle private classes.
    // Assume format of csv file is rigidly defined - for further flexibility, the header row could define column orderings. Would also need to tie column names to properties of entities somehow.
    public class StockValueCsvRealizer : ILineRealizer<StockValue>
    {
        enum StockValueCsvOrdinals
        {
            IndexName, Date, StockId, Name, Price, NumberOfShares
        }
        public StockValue ParseLine(string line)
        {
            //TODO validation 
            var vals = line.Split(',');
            var rtn = new StockValue
            {
                Date = DateTime.Parse(vals[(int)StockValueCsvOrdinals.Date]),
                IndexName = vals[(int)StockValueCsvOrdinals.IndexName],
                NumberOfShares = int.Parse(vals[(int)StockValueCsvOrdinals.NumberOfShares]),
                Price = decimal.Parse(vals[(int)StockValueCsvOrdinals.Price]),
                Name = vals[(int)StockValueCsvOrdinals.Name],
                StockId = vals[(int)StockValueCsvOrdinals.StockId],
            };

            rtn.TotalValue = rtn.Price * rtn.NumberOfShares;

            return rtn;
        }
    }
}