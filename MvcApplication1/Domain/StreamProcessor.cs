﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.IO;
using System.Linq;
using System.Web;

namespace Hsbc.Domain
{

    // This class knows how to read a stream. For every row, call a method
    // todo: Code to interface instead of concrete class (StreamReader) 
    public class StreamProcessor<T> where T : class
    {            
        public void ProcessStream(ILineRealizer<T> realiser, StreamReader reader, Action<T> action, bool firstLineIsHeader = true)
        {
            bool isFirstLine = true;
            while (!reader.EndOfStream)
            {
                if (firstLineIsHeader && isFirstLine)
                {
                    reader.ReadLine();
                    //ignore result
                }
                else
                {
                    action(realiser.ParseLine(reader.ReadLine()));
                }
                isFirstLine = false;
            }
        }
    }
}