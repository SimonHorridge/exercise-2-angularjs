﻿namespace Hsbc.Domain
{
    public interface ILineRealizer<T>
    {
        T ParseLine(string line);
    }
}