﻿using Hsbc.DataAccess.Entities;
using System;
using System.IO;

namespace Hsbc.Domain
{
    public class CapitalizationWeightedIndexService
    {
        public enum FileSourceType { 
            Csv,
            SomeOtherTypesNotUsedInThisExample        
        }


        // todo -make this generic -eg ProcessFile(string fileName, Action<T> action, )
        public void ProcessFile(string fileName, Action<StockValue> action, FileSourceType fileSourceType)
        {
         var realiser = new StockValueCsvRealizer();  // TODO -  use facotry to get realizer for source file type
           using (var reader = new StreamReader(fileName))
            {
                var processor = new StreamProcessor<StockValue>();
                processor.ProcessStream(realiser, reader, action);
            }
        }
    }
}