﻿using System;
using System.Collections.Generic;
using System.IO;
using Hsbc.Controllers;
using Hsbc.DataAccess.Entities;
using Hsbc.Domain;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Hsbc.Tests
{
    [TestClass]
    public class CapitalizationTest
    {
        [TestMethod]
        public void ReadLine()
        {
            var realiser = new Domain.StockValueCsvRealizer();
            var cap = realiser.ParseLine("FTRND,2015-01-01,98OLSGD,StockA,1990,6179");
            Assert.IsNotNull(cap);
            Assert.AreEqual(cap.IndexName, "FTRND");
            Assert.AreEqual(cap.Date, new DateTime(2015, 01, 01));
            Assert.AreEqual(cap.StockId, "98OLSGD");
            Assert.AreEqual(cap.Name, "StockA");
            Assert.AreEqual(cap.Price, 1990);
            Assert.AreEqual(cap.NumberOfShares, 6179);
            Assert.AreEqual(cap.TotalValue, 6179 * 1990);
        }
        //TODO: add tests for bad data

        [TestMethod]
        public void TestReadFile()
        {
            // TODO : negative tests.
            // todo: location for test data 

            var list = new List<StockValue>();
            var service = new CapitalizationWeightedIndexService();
            service.ProcessFile("..\\..\\Data\\StockDataSmall.csv", x => list.Add(x));
            Assert.AreEqual(list.Count, 3);
        }
        [TestMethod]
        public void TestController()
        {

        }   
    }
}
